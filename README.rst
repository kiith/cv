==================
Ferdinand Majerech
==================

-------
Contact
-------

* **Email**:  kiithsacmp@gmail.com
* **GitLab**: https://gitlab.com/kiith
* **GitHub**: https://github.com/kiith-sa

------------------
Employment history
------------------

* Spring 2016 - Present: **Software Engineer: Network Security** at **ESET**

  - Low-level network programming, custom packet processing compiler development (C++14/Linux/LLVM/BPF)

* Summer 2015 - Spring 2016: **Various C/C++ SH4/ARM projects** at **Antik Technology**

  - Smart home control platform (C++11/SH4)
  - Transcoder unit (Python, C++11/ARMv7)
  - OTA-update through MPEG/TS DSM-CC data carousels (C99/SH4)
  - other, minor projects
  
* June 2013 - November 2013: **A graph database system at SAGE team, s.r.o.**
 
  Was intended to be the backbone of an unconventional web framework. Written in C++.
  Project aborted due to investor departure.
  
--------------------
Open source projects
--------------------

* Spring 2018 - Present: **smac-ng**
  
  Work-in-progress open source game engine for Sid Meier's Alpha Centauri (a TBS game)
  
  - Source code: https://gitlab.com/v01d-sk/smac-ng

* Winter 2014 - 2016: **Harbored-mod**

  A documentation generator for the D programming language with DDoc and 
  Markdown support.

  - Source code: https://github.com/kiith-sa/harbored-mod
  - Generated documentation example: http://defenestrate.eu/docs/tharsis-core/api/tharsis/entity/entitymanager/EntityManager.html

* Autumn 2014 - 2016: **Tharsis.prof** / **Despiker**

  An open-source real-time frame-based profiler (inspired by RAD Telemetry),
  profiling library and GUI (OpenGL + SDL) frontend.

  - Source code (Tharsis.prof): https://github.com/kiith-sa/tharsis.prof
  - Source code (Despiker): https://github.com/kiith-sa/despiker
  - Tutorial & Video (Despiker): http://defenestrate.eu/docs/despiker/tutorials/getting_started.html
  - Frame-based profiling articles:

    * http://defenestrate.eu/2014/09/05/frame_based_game_profiling.html
    * http://defenestrate.eu/2014/09/05/optimizing_memory_usage_of_a_frame_based_profiler.html
    * http://defenestrate.eu/2014/09/05/frame_based_profiling_with_d_ranges.html

* October 2013 - 2016: **Tharsis**

  Masters thesis project.
  An entity-component framework designed for automated threading of 
  game/simulation logic.

  - Source code: https://github.com/kiith-sa/tharsis-core, https://github.com/kiith-sa/tharsis-full

* 2012 - 2013:    **Awesome2D**

  A 2D graphics engine showcasing advanced 2D
  lighting techniques.

  - Source code: https://github.com/kiith-sa/awesome2D

* 2010 - 2012:    **ICE**

  A moddable vertical shooter game written in D inspired by games such
  as Tyrian and Raptor.

  - Site: http://icegame.nfshost.com/
  - Documentation: http://icegame.nfshost.com/doc/html/index.html
  - Source code: https://github.com/kiith-sa/ICE

* 2011 - present: **D:YAML**

  YAML parser and emitter written in D.

  - Documentation: http://ddocs.org/dyaml/latest/index.html
  - Source code: https://github.com/kiith-sa/D-YAML

* 2009 - 2010:    **MiniINI**

  A high-performance INI file parser in C++.  It was created as
  a challenge to create the fastest INI parser possible, and as such sacrifices
  API usability as well as code readability for performance.

  - Site: http://miniini.tuxfamily.org/
  - Documentation: http://miniini.tuxfamily.org/docs.php
  - Source code: http://bazaar.launchpad.net/~kiithsacmp/miniini/trunk/files

* Other:

  - Various small D libraries
  - Various unreleased C++ game projects

^^^^^^^^
Non-code 
^^^^^^^^

* **IHRA**: http://web.ics.upjs.sk/ihra

  Website, communication with game developers, secondary organization tasks; led 
  by Lubomir Antoni

* **v01d hackerspace**: http://v01d.sk

  Helping maintain the space, occasional talks & projects, as part of the local hacker
  community


------
Skills
------

* Human languages:

  - Slovak/Czech (fluent)
  - English      (fluent)

* Programming languages:

  - Bash             (beginner)
  - C                (intermediate)
  - C++ (incl. STL)  (advanced)
  - D                (advanced)
  - Python           (intermediate)
  - Other            (GLSL, x86 ASM, Java, SQL, Pascal, PHP, Vala ...)

**Tools**

* Linux development and administration

  - ssh
  - tmux
  - Experience with Debian, Mint, Ubuntu, OpenSUSE, Puppy, OpenWRT ...

* Any IDE/editor (Experience with Code::Blocks, Qt Creator, KDevelop, Eclipse...)

  - Vim with a custom plugin configuration preferred

* Any VCS (git, bzr, svn, etc.)

  - Git preferred
  - Feature branch workflow in Git and other DVCS's
  - Continous integration (used Travis CI)
  - Bisect VCS history to find bug sources (e.g. ``git bisect``)

* Compilers

  - GCC
  - LLVM/Clang

* Any issue tracking (RedMine, JIRA, Trac, GitHub, Gitorious...)

* Documentation

  - Doxygen, DDoc, JavaDoc, etc.
  - Sphinx/ReStructuredText, Markdown, etc.
  - LaTeX

* Debugging

  - GDB
  - valgrind: memcheck
  - strace/ltrace

* Profiling

  - perf
  - valgrind: cachegrind, callgrind, massif
  - oprofile
  - APITrace (for OpenGL)

**Code quality**

* Documenting code before commit

* Performance/Optimization

  - Algorithms and data structures (complexity)
  - Statistical and instrumentation-based profiling
  - Machine architecture understanding (cache performance, branch prediction, etc.)
  - Roll-your-own profiling (high precision clocks, custom allocators)

* Debugging

  - Finding memory leaks, uninitialized data usage with valgrind
  - Debugging syscall/library usage with strace/ltrace
  - Traditional step-by-step debugging
  - Small commits + binary searching VCS history (e.g. git bisect)

* Defensive programming

  - Contracts & invariants
  - Unittests
  - Const correctness, functional purity


**Domain-specific**

* Embedded with Linux (beginner)

  - ARM (v7, v8)
  - SH4

* Crossplatform libraries/frameworks (intermediate)

  - Boost
  - Qt
  - SDL
  - NCurses
  - GTK/Vala

* Graphics programming (advanced)

  - OpenGL 1-4.x
  - OpenGL ES 2.x

* Low-level network programming (intermediate) 

  - TCP/IP stack programming
  - Packet capture: NFQUEUE, libpcap, AF_PACKET, (beginner) DPDK
  - BPF/eBPF

* Compiler development (intermediate):

  - LLVM
  

---------
Interests
---------

* See *Skills*
* 3D graphics
* Literature, PC gaming, film
* Modding
* Astronomy, cosmonautics
* Cooking

---------
Education
---------

* 2013 - 2015:    Univezita Pavla Jozefa Šafárika v Košiciach
                  Faculty of Science
                  Informatics
                  Master's
* 2010 - 2013:    Univezita Pavla Jozefa Šafárika v Košiciach
                  Faculty of Science
                  Informatics
                  Bachelor's
* 2006 - 2010:    Gymnázium Školská 7, Spišská Nová Ves
